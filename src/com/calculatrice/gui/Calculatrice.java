package com.calculatrice.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculatrice extends JFrame implements ActionListener {
    private String premierNombre;
    private String operateur;
    private String deuxiemeNombre;
    private JFrame f;
    private JTextField tf;

    public Calculatrice() {
        this.premierNombre = "";
        this.operateur = "";
        this.deuxiemeNombre = "";
    }

    private static Double arrondir(double d, int nbDecimales) {
        if (nbDecimales < 0) {
            System.err.println("Impossible d'arrondir à " + nbDecimales + " décimales");
            return d;
        }

        BigDecimal bd = BigDecimal.valueOf(d);
        bd = bd.setScale(nbDecimales, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private static Double calculer(String premierNombre, String operateur, String deuxiemeNombre) {
        double resultat = (-1);
        double d1;
        double d2;
        char op = operateur.charAt(0);

        try {
            d1 = Double.parseDouble(premierNombre);
            d2 = Double.parseDouble(deuxiemeNombre);
        }

        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Erreur : parseDouble");
            return resultat;
        }

        switch (op) {
            case '+':
                resultat = d1 + d2;
                break;
            case '-':
                resultat = d1 - d2;
                break;
            case '*':
                resultat = d1 * d2;
                break;
            case '/':
                if (d2 == 0.) {
                    System.err.println("Erreur : division par zéro");
                }
                else {
                    resultat = d1 / d2;
                }

            default:
                break;
        }

        return arrondir(resultat, 2);
    }

    public void initialiser() {
        this.f = new JFrame("Calculatrice");
        this.f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }

        catch (Exception e) {
            System.err.println(e.getMessage());
        }

        this.tf = new JTextField(16);
        this.tf.setEditable(false);

        // Initialisation des boutons
        JButton zero, un, deux, trois, quatre, cinq, six, sept, huit, neuf;
        JButton egal, plus, moins, div, mult, clear, virgule;

        zero = new JButton("0");
        un = new JButton("1");
        deux = new JButton("2");
        trois = new JButton("3");
        quatre = new JButton("4");
        cinq = new JButton("5");
        six = new JButton("6");
        sept = new JButton("7");
        huit = new JButton("8");
        neuf = new JButton("9");

        egal = new JButton("=");
        plus = new JButton("+");
        moins = new JButton("-");
        div = new JButton("/");
        mult = new JButton("*");
        clear = new JButton("C");
        virgule = new JButton(".");

        // Ajout des Action Listener
        mult.addActionListener(this);
        div.addActionListener(this);
        moins.addActionListener(this);
        plus.addActionListener(this);
        neuf.addActionListener(this);
        huit.addActionListener(this);
        sept.addActionListener(this);
        six.addActionListener(this);
        cinq.addActionListener(this);
        quatre.addActionListener(this);
        trois.addActionListener(this);
        deux.addActionListener(this);
        un.addActionListener(this);
        zero.addActionListener(this);
        clear.addActionListener(this);
        egal.addActionListener(this);

        // Création et ajout des éléments au panel
        JPanel p = new JPanel();
        p.setBackground(Color.black);
        p.add(tf);
        p.add(plus);
        p.add(un);
        p.add(deux);
        p.add(trois);
        p.add(quatre);
        p.add(cinq);
        p.add(six);
        p.add(mult);
        p.add(sept);
        p.add(huit);
        p.add(neuf);
        p.add(div);
        p.add(virgule);
        p.add(zero);
        p.add(clear);
        p.add(egal);

        // Ajout du JPanel au JFrame
        this.f.add(p);
        this.f.setSize(200, 220);
        this.f.setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        char c = s.charAt(0);
        double resultat;

        if (Character.isDigit(c)) {
            /*
             * S'il y a déjà un opérateur, on l'ajoute au 2è nombre Sinon, on l'ajoute au
             * premier
             */

            if (!operateur.isEmpty()) {
                deuxiemeNombre += s;
            }
            else {
                premierNombre += s;
            }

            tf.setText(premierNombre + operateur + deuxiemeNombre);
        }

        else if (c == 'C') {
            premierNombre = "";
            operateur = "";
            deuxiemeNombre = "";
            tf.setText(premierNombre + operateur + deuxiemeNombre);
        }

        else if (c == '=') {
            resultat = calculer(premierNombre, operateur, deuxiemeNombre);
            tf.setText(premierNombre + operateur + deuxiemeNombre + "=" + resultat);
            premierNombre = Double.toString(resultat);
            operateur = "";
            deuxiemeNombre = "";
        }

        else {
            if (operateur.isEmpty() || deuxiemeNombre.isEmpty()) {
                operateur = s;
            }

            else {
                resultat = calculer(premierNombre, operateur, deuxiemeNombre);
                premierNombre = Double.toString(resultat);
                operateur = s;
                deuxiemeNombre = "";
            }

            tf.setText(premierNombre + operateur + deuxiemeNombre);
        }
    }
}
